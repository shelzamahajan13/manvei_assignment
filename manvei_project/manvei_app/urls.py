from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.static import static
from .import views

urlpatterns = [
    url(r'^$', views.HomePageOnload, name=''),
    url(r'^reg/$', views.RegistrationPageOnload, name='reg'),
    url(r'^login/$', views.LoginPage, name='login'), 
    url(r'^registeration_user_data/$', views.registeration_user_data, name='update_personnel_data'),
    url(r'^check_login_user_credentials/', views.check_login_user_credentials, name='check_login_user_credentials'),
    url(r'^logout_view/$', views.logout_view,name='logout_view'),
    url(r'^dashboard/$', views.DashboardPageOnload, name='dashboard'), 
    url(r'^fetch_passport_number/(?P<data>[a-zA-Z0-9_#&%@*,():{}" ".|+-]+)$', views.fetch_passport_number,name='date_time_value_function'),
    url(r'^change-password/$', views.ChangePasswordOnload, name='change-password'), 
    url(r'^date_time_value_function/(?P<data>[a-zA-Z0-9_#&%@*,():{}" ".|+-]+)$', views.date_time_value_function,name='date_time_value_function'),
    url(r'^verification_old_password_system/(?P<data>[a-zA-Z0-9_%@,:{}" ".-]+)$', views.verification_old_password_system, name='dashboard_details'),
    url(r'^update_user_password/$', views.update_user_password, name='company_data'),
]   
# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
