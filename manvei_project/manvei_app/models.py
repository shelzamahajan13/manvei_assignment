from django.db import models
from django.utils import timezone
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver




class PassportDummyDataStatistics(models.Model):
	name=models.CharField(max_length=200,null=True,blank=True)
	mobile=models.CharField(max_length=200,null=True,blank=True)
	passport_no=models.CharField(max_length=200,null=True,blank=True)
	city=models.CharField(max_length=200,null=True,blank=True)
	state=models.CharField(max_length=200,null=True,blank=True)

class UserRegisterationDetails(models.Model):
	auth_user = models.ForeignKey(User, on_delete=models.CASCADE)
	name=models.CharField(max_length=200,null=True,blank=True)
	mobile=models.CharField(max_length=200,null=True,blank=True)
	email_id=models.CharField(max_length=100, null=True,blank=True)
	passport_number = models.CharField(max_length=100,null=True,blank=True)
	dob = models.DateField(null=True,blank=True)
	age = models.CharField(max_length=100,null=True,blank=True)
	image=models.FileField(upload_to='media',null=True,blank=True)
	password=models.CharField(max_length=100,null=True,blank=True)
	user_registered_date=models.DateTimeField('user_registered_date',null=True,blank=True)