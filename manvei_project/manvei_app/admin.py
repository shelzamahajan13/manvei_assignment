
from django.contrib import admin
from import_export import resources

from .models import UserRegisterationDetails,PassportDummyDataStatistics
from import_export.admin import ImportExportModelAdmin

class UserRegisterationDetailsAdmin(admin.ModelAdmin):
	list_display = ('id','auth_user','name', 'mobile','email_id', 'passport_number','dob','age','image','password','user_registered_date')

	search_fields = ('id','auth_user','name', 'mobile','email_id', 'passport_number','dob','age','image','password','user_registered_date')


	row_id_fields=('auth_user',)
	
	def User(self, obj):
		return obj.auth_user.user

class PassportDummyDataStatistics_Import(resources.ModelResource):
	class Meta:
		model = PassportDummyDataStatistics
		fields = ('id','name', 'mobile','passport_no','city', 'state')

class PassportDummyDataStatisticsAdmin(ImportExportModelAdmin):
	list_display = ('id','name', 'mobile','passport_no','city', 'state')
	search_fields = ('id','name', 'mobile','passport_no','city', 'state')
	resource_class=PassportDummyDataStatistics_Import




admin.site.register(UserRegisterationDetails,UserRegisterationDetailsAdmin)
admin.site.register(PassportDummyDataStatistics,PassportDummyDataStatisticsAdmin)