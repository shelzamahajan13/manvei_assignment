
var app = angular.module('myApp', ['ui.utils.masks','naif.base64']);

app.config(function ($interpolateProvider) {


    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});



domainName = document.location.origin;
console.log(domainName)

app.constant('CONFIG', {
    'APP_NAME' : 'My Awesome App',
    'APP_VERSION' : '0.0.0',
    'GOOGLE_ANALYTICS_ID' : '',
    'BASE_URL' : domainName+'/',
    'SYSTEM_LANGUAGE' : '',
    'FILE_PATH':domainName+'/static/img/uploads/'
})

$(window).on("load", function () {
     $(".loader").addClass('animated heartBeat slow infinite ').fadeOut('slow');
});

(function (module) {
     
    var fileReader = function ($q, $log) {
 
        var onLoad = function(reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };
        var onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };
 
        var onProgress = function(reader, scope) {
            return function (event) {
                scope.$broadcast("fileProgress",
                    {
                        total: event.total,
                        loaded: event.loaded
                    });
            };
        };
        var getReader = function(deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };
        var readAsDataURL = function (file, scope) {
            var deferred = $q.defer();
             
            var reader = getReader(deferred, scope);         
            reader.readAsDataURL(file);
             
            return deferred.promise;
        };
 
        return {
            readAsDataUrl: readAsDataURL  
        };
      };
      module.factory("fileReader",
                   ["$q", "$log", fileReader]);
 
}(angular.module("myApp")));




app.directive('customFilterValidation', function () {
      return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
          function fromUser(text) {
            var transformedInput = text.replace(/\s/g, '');
            
            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          ngModelCtrl.$parsers.push(fromUser);
        }
      };
});



// registeration time
app.directive("ngFile1Select",function(){

    return {
        link: function($scope,el){
          
          el.bind("change", function(e){
          
            $scope.file = (e.srcElement || e.target).files[0];
            var allowed = ["jpeg", "png", "gif", "jpg"];
            var found = false;

            var img;

            img = new Image();
            allowed.forEach(function(extension) {
                    if ($scope.file.type.match('image/'+extension)) {
                     found = true;
                    }
            });
        
            if(found){
                   
                        $(".loader").show()
                        $scope.getFile18();
       
                    
            }else{
                $.alert({
                    icon: 'fa fa-exclamation-circle',
                    theme: 'white',
                    closeIcon: true,
                    animation: 'scale',
                    type: 'red',
                    title: 'Alert!',
                    content: 'File type should be .jpeg, .png, .jpg, .gif',
                })
                return ;
            }

           
           
          })
      
        }
    
    }
})






