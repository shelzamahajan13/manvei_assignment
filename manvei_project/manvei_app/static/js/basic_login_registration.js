
app.controller("registration_controller",function($scope,$http,CONFIG,$timeout,$interval,fileReader){
	var BASE_URL=JSON.stringify(CONFIG['BASE_URL']).replace(/\"/g, "");
	var FILE_PATH=JSON.stringify(CONFIG['FILE_PATH']).replace(/\"/g, "");
	
	// validation functions
	$scope.validation=function(type,value){
		
		if(type == "name"  && value!== undefined){
			if(value!=''){
				if(type == "name"){
					$scope.fullnameerr= ""; 
					return true;
				}
			}else{
				$scope.fullnameerr = "Enter full name";
				return false;
			} 
		}else if((type == "name") && value == undefined){
				$scope.fullnameerr = "Full name required";
				return false;
		}
		
			
		
		if(type == "mobile_no"  && value!== undefined){
			
			if(/^[6789]\d{9}$/.test(value) && value.length ==10){
				if(type == "mobile_no"){
					$scope.mobile_err= ""; 
					return true;
				}

			}else{
				$scope.mobile_err = "Enter 10 digit mobile number";
				return false;
			} 
		}else if((type == "mobile_no") && value == undefined){
				$scope.mobile_err = "Mobile number required";
				return false;
		}

		if(type == "email_id" && value!== undefined) {
			if(/^([a-zA-Z0-9])+([a-zA-Z0-9._%+-])+@([a-zA-Z0-9_.-])+\.(([a-zA-Z]){2,6})$/.test(value)){
				if(type == "email_id"){
					$scope.email_err = ""; 
					return true;
				}
			}else{
					
				$scope.email_err = "Enter email address";
				return false;}
		}else if((type == "email_id") && value === undefined){
					$scope.email_err = "Email address required";
					return false;
		}


		if(type == "date_birth" && value!=undefined){
		
			if(value!==''){
				var valid=$scope.check_date(value);
				
				if(valid==false){
					$scope.date_iderr= "Enter valid date of birth"; 
					return false;
				}else if(valid==true){
					var age=$scope.calculate_age(value);
	                if(age<16){
	                	$scope.date_iderr= "Age should be greater than 16"; 
	               	 	return false;
	              	}else{
	                	$scope.date_iderr= "";
	                	return true;
	              	}
	            }      
					
			}else{
				$scope.user.age=''
				$scope.date_iderr= "Enter date of birth"; 
				return false;
			}
		}else if(type == "date_birth" && value==undefined){
			$scope.date_iderr= "Enter date of birth"; 
			return false;
		}

		if(type == "passport_no" && value!== undefined) {

			if(/[a-zA-Z]{1}[0-9]{7}/.test(value) ){
				if(type == "passport_no"){
					$scope.passport_no_err = ""; 
					return true;
				}
			}else{
					
				$scope.passport_no_err = "Enter passport number";
				return false;
			}
		}else if((type == "passport_no") && value === undefined){
					$scope.passport_no_err = "Passport address required";
					return false;
		}

		if(type == "password"  && value!== undefined){
			if(value!=''){
				if(type == "password"){
					$scope.passwordid_err= ""; 
					return true;
				}
			}else{
				$scope.passwordid_err = "Enter password";
				return false;
			} 
		}else if((type == "name") && value == undefined){
				$scope.passwordid_err = "Password required";
				return false;
		}

		
	}


	$scope.check_date=function(value){
		check_date=value.split('/');
		date=check_date[0];
		month=check_date[1];
		year=check_date[2];


		var dateCheck = /^(0?[1-9]|[12][0-9]|3[01])$/;
		var monthCheck = /^(0[1-9]|1[0-2])$/;
		// var yearCheck = /^\d{4}$/; 
		var yearCheck =/^\b(19|[2-9][0-9])\d{2}$/; 

		if (month.match(monthCheck) && date.match(dateCheck) && year.match(yearCheck)){
			var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			if (month == 1 || month > 2) {
				if (date > ListofDays[month - 1]) {
					return false;
				}
			}
			if (month == 2) {
				var leapYear = false;
				if ((!(year % 4) && year % 100) || !(year % 400)) {
								leapYear = true;
				}
				if ((leapYear == false) && (date >= 29)) {
								return false;
				}
				if ((leapYear == true) && (date > 29)) {
								return false;
				}

			}
			return true;

		}
		else{
			return false;

		}
	}

	$scope.calculate_age=function(value){

	    check_date=value.split('/');

	    date=check_date[0];
	    month=check_date[1];
	    year=check_date[2];

	    var dateString=year+'/'+month+'/'+date

	    var today = new Date();
	    var birthDate = new Date(dateString);
	    var age = today.getFullYear() - birthDate.getFullYear();
	    var m = today.getMonth() - birthDate.getMonth();
	    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
	        age--;
	        
	    }
	    $scope.user.age=age
	    return age;
	}

	$scope.fetch_passport_number=function(mobile_no){
		
		if(mobile_no.length==10 && $scope.user.name!=''){
			var data=JSON.stringify({"mobile_no":mobile_no,'name':$scope.user.name})
			$http.post(BASE_URL+"fetch_passport_number/"+data).then(function(response){
				var result_value=JSON.stringify(response.data.a_res).replace(/\"/g, "");
				if(result_value=='0'){
					var value=JSON.stringify(response.data.status).replace(/\"/g, "");
					if(value=='0'){
						$scope.user.passport_no=JSON.stringify(response.data.get_passport_number).replace(/\"/g, "");
						$("#passport_no").addClass('disabled')
					}else{
						$scope.user.passport_no=''
						$("#passport_no").removeClass('disabled')

					}
				}else{
					$scope.mobile_err = "Mobile Number already exists";
					$("#passport_no").addClass('disabled')
					return false;
				}
			},function(response){
				console.log("error")
			})
		}
	}

	$(".thumbnail").hide()
	$scope.getFile18 = function () {
	    $scope.progress = 0;

        fileReader.readAsDataUrl($scope.file, $scope)
                      .then(function(result) {
                      	  $(".loader").hide()
                      	  $(".thumbnail").show()
                          $scope.update_photo1 = result;
                          $scope.imageerr1 =''
                          
                      });
    };

	// submit user registration data
	$scope.registeration_user_data=function(user){
		var name=$scope.validation('name',$scope.user.name);
		var mobile=$scope.validation('mobile_no',$scope.user.mobile_no);
		var email_id=$scope.validation('email_id',$scope.user.email_id);
		var passport_no=$scope.validation('passport_no',$scope.user.passport_no);
		var date_birth=$scope.validation('date_birth',$scope.user.date_birth);

		if($scope.euin1_f=='' || $scope.euin1_f==undefined){
			
            var update_profile_image2={'euin1_f':''};
           

            if($scope.update_photo1==undefined){
            	Object.assign(user,euin1_f);

                $scope.euin1_f="";

            }else{
            	var filename = $scope.update_photo1.split('/').pop()
				
				$scope.euin1_f=filename
            }

            
            var update_image_data2=JSON.stringify($scope.euin1_f);
        }else{
        	
                var file=$scope.euin1_f;
                var filename=JSON.stringify(file['filename']);
                var euin1_f={'euin1_f':filename};
                Object.assign(user,euin1_f);
                if($scope.euin1_f!='' || $scope.euin1_f!=undefined){
                    var update_image_data2=JSON.stringify($scope.euin1_f);
                }
        }
		var data=angular.copy(user)
		var data=JSON.stringify(data)
		
		if(name==true && mobile==true && email_id==true &&  passport_no==true && date_birth==true){
			$(".loader").show()
			$.ajax({
				url:'/registeration_user_data/',
				type:'POST',
				dataType: 'json',
				data:{'data':data,"update_image_data2":update_image_data2},
				success: function(response){
					$(".loader").hide()
					var a=response['status'];
					
					if(a=="success"){
						var result_set=response['a_res'];
						if(result_set=='0'){
							$.confirm({
					            theme: 'modern',
					            columnClass: 'medium',
					            title: 'Congratulations!!',
					            content: 'Your account has been registered successfully & Login credentials have been emailed!<br>',
					            type: 'green',
					            animation: 'RotateYR',
					            closeAnimation: 'RotateYR',
					            typeAnimated: true,
					            buttons: {
					              Cancel: function () {
					                location.reload()
					              },
					              Login: function () {
					                location.href = BASE_URL+'login/'
					              },

					            }
					        });

				        
			  		 		$(".empty_value").val('')

						}else if(result_set=='1'){
							$.alert({
					            icon: 'fa fa-exclamation-circle',
					            theme: 'white',
					            animation: 'scale',
					            type: 'red',
					            title: 'Error!',
					            content: 'Error in sending mail.',
				        	})
						}else{
							$.alert({
					            icon: 'fa fa-exclamation-circle',
					            theme: 'white',
					            animation: 'scale',
					            type: 'red',
					            title: 'Error!',
					            content: 'Error in uploading image on AWS S3.',
				        	})
						}
						
				        
			  		 	
					}
					
					if(a=='exists'){
						
						$.alert({
				            icon: 'fa fa-exclamation-circle',
				            theme: 'white',
				            
				            animation: 'scale',
				            type: 'red',
				            title: 'Alert!',
				            content: 'Email id already exist.',
				        })
					}
				},function (response) {
					if(response.status=="500"){
						console.log('wrong1'); 
					}
				}
			})
		}
	}

	
    $scope.inputType='password';

    $scope.show_password=function(){
		if($scope.user.password!=null){
		
			if($scope.inputType=='password'){
					$scope.inputType='text'
					$scope.user.password_value=true
			}else{
					$scope.inputType='password';
					$scope.user.password_value=false
			}
		}
	}

    $scope.check_login_user_credentials=function(user){
    	
    	var mobile=$scope.validation('email_id',$scope.user.email_id)
		var password=$scope.validation('password',$scope.user.password)
		
		if(mobile==true && password==true){
			
			var data=angular.copy({"email_login":$scope.user.email_id,"password":$scope.user.password})
			var data=JSON.stringify(data)

			$(".loader").show()
			$.ajax({
				url:'/check_login_user_credentials/',
				type:'POST',
				dataType: 'json',
				data:{'data':data},

				success: function(response){
					$(".loader").hide()
					var a=response['status']
					user_id = response['user_id'];
					
					if(a=='success'){
						
						$('.login_success').html('<p class="alert alert-success" style="color: black;border-radius:15px"><strong>Success!</strong>Login Successfully.</p>');
						$('.username_err').html('');
						window.setTimeout(function(){
	                        location = BASE_URL+"dashboard/"
	                     },0)
						
					}else if(a=='wrong'){
						$('.username_err').html('<p class="alert alert-danger error_color" style="border-radius:15px">Please enter valid password.</p>');
						$('.login_success').html('')
						$("#password").val('')
					}else if(a=='email_mobile'){
						$('.username_err').html('<p class="alert alert-danger error_color" style="border-radius:15px">Email id not exist.</p>');
						$('.login_success').html('')
						$("#password").val('')
						$("#email_mob_login").val('')
					}

					
					
				},function (response) {
					if(response.status=="500"){
						console.log('wrong1'); 
					}
				}


			})
		}
			
		
    }

    
	


});