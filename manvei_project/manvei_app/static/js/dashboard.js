

function openNav() {
	 $('#mySidebar').show();
	 $('.overlay_sec').addClass('animated slideInRight');
	 setTimeout(function(){ $('.overlay_sec').removeClass('animated slideInRight'); }, 1000);

 }
 function closeNav() {
 
	 $('.overlay_sec').addClass('animated slideOutRight');
	  setTimeout(function(){  
	  $('#mySidebar').hide();
	  $('.overlay_sec').removeClass('animated slideOutRight');
	  }, 1000);
}




app.controller("dashboard_data_controller",function($scope,$http,CONFIG,$timeout,$interval){
	var BASE_URL=JSON.stringify(CONFIG['BASE_URL']).replace(/\"/g, "");

	var tick = function() {
		var current_date=new Date();
	    $scope.clock =current_date;
	    var get_hours=current_date.getHours();
	    var TimeOut_Thread = $timeout(function(){ $scope.date_time_value_function(get_hours) } , 100);
	}
	tick();
	$interval(tick, 500);

	$scope.date_time_value_function=function(get_hours){
		
	
		var data=JSON.stringify({"get_hours":get_hours})
		
		$http.post(BASE_URL+"date_time_value_function/"+data).then(function(response){

			$scope.greeting_name=JSON.stringify(response.data.greeting_name).replace(/\"/g, "")
			
		},function(response){
			console.log("error")
		})
		
    	
	}

  

  $scope.verification_old_password=function(password_value,user_id){
    	
      if(password_value==''){
        $(".message_display").html('<span style="color:red"> </i> Enter old password </span>')
        $(".email_field_hide").addClass('disabled')
      	$(".message_display").show()
      }
      else{
	      var verify_data={"password_value":password_value,"user_id":user_id}
	      var data=JSON.stringify(verify_data)
	      
	      
	      $http.post(BASE_URL+'verification_old_password_system/'+data).then(function(response){
	        var res_val=JSON.stringify(response.data.status).replace(/\"/g, "");
	        
	        if(res_val=='0'){
		        var res_val1=JSON.stringify(response.data.status1).replace(/\"/g, "");
		        if(res_val1=='valid'){
		            $(".message_display").html('<span style="color:green;font-size:12px"> <i class="fa fa-check-circle"  aria-hidden="true"></i> Password verified successfully. </span>')
		            $(".email_field_hide").removeClass('disabled')
		            
		            setTimeout(function(){
					  	$(".message_display").html(' ')
					}, 800)
		        }else{
		            $(".message_display").html('<span style="color:red;font-size:12px"> <i class="fa fa-times-circle"  aria-hidden="true"></i> Wrong password </span>')
		            $(".email_field_hide").addClass('disabled')
		            
		             setTimeout(function(){
					  	$(".message_display").html(' ')
					}, 800)
		        }
	        }else{
	          $(".message_display").html('<span style="color:red;font-size:12px"> <i class="fa fa-times-circle-o"  aria-hidden="true"></i> Server error occured. </span>')
	          $(".email_field_hide").addClass('disabled')
	        }
	        
	      },function(response){
	        console.log("error")
	      })
	      
   	 }
  }

  $scope.validation=function(type,value){
	

	if(type == "password"  && value!== undefined){
		if(value!=''){
			if(type == "password"){
				$scope.passwordid_err= ""; 
				return true;
			}
		}else{
			$scope.passwordid_err = "Enter password";
			return false;
		} 
	}else if((type == "name") && value == undefined){
			$scope.passwordid_err = "Password required";
			return false;
	}

	if((type == "conpassword") && (value!==undefined)){ 
            if(value!=''){
                if(type == "conpassword"){
                  $scope.passwordid1_err= "";
                  return true;
                }
            }else{ 
            	if(type == "conpassword"){
              		$scope.passwordid1_err= "Please confirm your password"; 
              		return false;
            	}
        	}
    }else if((type == "conpassword") && (value==undefined)){
            if(type == "conpassword"){
              $scope.passwordid1_err= "Confirm password required"; 
              return false;
            }
    } 

}

$scope.password_matching_validation=function(){

   	data1= $("#password").val();
   	
   	data=$("#conpassword").val()
   	
    if(data=='' && data1==''){
	     return false;
    }
    else if(data==data1){
    	  return true;
    }else if (data!=data1){
	    $.alert({
          icon: 'fa fa-warning',
            title: 'Error!',
            
            content: 'Your password do not match. <br>Please try again.',
          type: 'red',
            typeAnimated: true,
      	});
      $("#password").val('')
      $("#conpassword").val('')
      return false;
    }
}


$scope.update_user_password=function(user){
	var disabled_result=$('.email_field_hide').is(':disabled')
	if(disabled_result==false){
		var password1=$scope.validation('password',$("#password").val())
		var conpassword1=$scope.validation('conpassword',$("#conpassword").val())
		var result=$scope.password_matching_validation()
		
		var user_id=$scope.user.user_id
		
		var password=$("#password").val()
		var data={"user_id":user_id,"password":password}
		var data=JSON.stringify(data)
		if(result==true && password1==true && conpassword1==true ){
			$(".loader").show()
			$.ajax({
				url:'/update_user_password/',
				type:'POST',
				dataType: 'json',
				data:{'data':data,},
				success: function(response){
					$(".loader").hide()
					var a=response['status']
					if(a=='success'){
						$.alert({
				            icon: 'fa fa-check',
				            theme: 'white',
				            
				            animation: 'scale',
				            type: 'green',
				            title: 'Success!!',
				            content:  'Your password has been changed successfully.',
				            buttons:{
				            	Ok:function(){
				            		window.location.href=BASE_URL+"logout_view/"
				            	}
				            }
				        });  
					  	
					}
					if(a=='error'){
						$.alert({
				            icon: 'fa fa-warning',
				            theme: 'white',
				           
				            animation: 'scale',
				            type: 'red',
				            title: 'Error!',
				            content: 'Server error occured. Please contact the Admin Team',
				            buttons:{
				            	Ok:function(){
				            		location.reload()
				            	}
				            }
			        	});
					}
					
					
					
				},function (response) {
					if(response.status=="500")
					{
						
						console.log('wrong1'); 
					}
					
				}
			})
		}
	}else{
		$(".message_display").html('')
	}
}



	
})