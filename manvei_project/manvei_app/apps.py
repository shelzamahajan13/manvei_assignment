from django.apps import AppConfig


class ManveiAppConfig(AppConfig):
    name = 'manvei_app'
