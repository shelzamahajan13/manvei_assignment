from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.views.generic import TemplateView,ListView
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

import logging
import requests
import json
import string,re
from django.contrib.auth import authenticate, login

import io
import sys
from django.utils import timezone
from datetime import datetime,timedelta
import datetime
from .models import UserRegisterationDetails,PassportDummyDataStatistics
from .static_file_path import *
from .email_sending_functions import *
from .s3_upload import *
import os

import base64
import urllib.request
import urllib.parse

from django import template
from django.template.loader import get_template
from django.urls import resolve
from strgen import StringGenerator as SG
import hashlib
stdlogger = logging.getLogger(__name__)

log = logging.getLogger(__name__)

if __name__ == '__main__':
	sys.exit(main())

# Create your views here.
def request_session_check(request):
	if 'user_id' in request.session:
		user_id = request.session['user_id']
		get_firstname=UserRegisterationDetails.objects.get(id=user_id).name
		get_current_date_value=current_time_function()
	
	else:
		user_id=''
		get_firstname=''
		
	return get_firstname+'|'+str(user_id)+'|'+get_current_date_value

def RegistrationPageOnload(request):
	get_key = request.session.has_key('user_id')
	if get_key!=False:
		try:
			data=request_session_check(request);
			data=data.split('|');
			return render(request,'registration.html',{
				'get_firstname':data[0].title(),
				'user_id':data[1],
				
			})
		except Exception as ex:
			log.error("Error in redirecting page: %s" % ex)
	else:
		user_id=''
		return render(request,'registration.html',{"user_id":user_id})

def HomePageOnload(request):
	get_key = request.session.has_key('user_id')
	if get_key!=False:
		try:
			data=request_session_check(request);
			data=data.split('|');

			return render(request,'home.html',{

				'get_firstname':data[0].title(),
				'user_id':data[1],
				
			})

		except Exception as ex:
			log.error("Error in redirecting page: %s" % ex)
	else:
		user_id=''
		return render(request,'home.html',{"user_id":user_id})


def LoginPage(request):
	get_key = request.session.has_key('user_id')
	
	if get_key!=False:
		try:
			data=request_session_check(request);
			data=data.split('|');

			return render(request,'login.html',{

				'get_firstname':data[0].title(),
				'user_id':data[1],
				
			})

		except Exception as ex:
			log.error("Error in redirecting page: %s" % ex)
	else:
		user_id=''
		return render(request,'login.html',{"user_id":user_id})

@csrf_exempt
def fetch_passport_number(request,data):
	fetch_result={}
	try:
		req_data=json.loads(data)
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)
	try:
		if request.method=="POST":
			if not UserRegisterationDetails.objects.filter(mobile=req_data['mobile_no']).exists():
				if PassportDummyDataStatistics.objects.filter(name=req_data['name'],mobile=req_data['mobile_no']).exists():
					get_passport_number=PassportDummyDataStatistics.objects.get(name=req_data['name'],mobile=req_data['mobile_no']).passport_no
					fetch_result['get_passport_number']=get_passport_number
					fetch_result['status']='0'
				else:	
					fetch_result['status']='1'
				fetch_result['a_res']='0'	
			else:
				fetch_result['a_res']='1'			
		return HttpResponse(json.dumps(fetch_result), content_type='application/json')
	except Exception as ex:
					log.error('Error in json data: %s' % ex) 


@csrf_exempt
def registeration_user_data(request):
	result={}
	try:
		req_data=request.POST.get('data')
		req_data=json.loads(req_data)
		password=SG("[\h]{8}").render()
		text=password.encode("utf-8")
		hash_password=hashlib.md5(text).hexdigest()
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)
	try:
		if request.method=="POST":
			dob=req_data['date_birth']
			if dob!='':
				d,m,y=dob.split("/")
				user_dob_format=y+'-'+m+'-'+d
				
			elif dob =='':
				user_dob_format=None
					
			cureent_register_timestamp= timezone.now()
			cureent_register_timestamp_new= cureent_register_timestamp +datetime.timedelta(hours=5,minutes=30)
			

			if not(User.objects.filter(username=req_data['email_id'])).exists():
				result_value=send_credentials_mail_user(gateway_email,str(req_data['email_id']),str(hash_password),str(req_data['name']).title())
				if result_value['status']=='success':
					User.objects.create_user(req_data['email_id'],req_data['email_id'],hash_password)
					atuser = User.objects.get(username=req_data['email_id'])
					
					UserRegisterationDetails.objects.create(auth_user=atuser,
						name=req_data['name'].strip(),
						mobile=req_data['mobile_no'].strip(),
						email_id=req_data['email_id'].strip(),
						passport_number=req_data['passport_no'].strip(),
						dob=user_dob_format,
						age=req_data['age'],
						password=hash_password,
						user_registered_date=cureent_register_timestamp_new,
						).save()

					get_user_register_instance=UserRegisterationDetails.objects.get(auth_user=atuser,email_id=req_data['email_id']).id

					profile_photo=request.POST.get('update_image_data2')
					
					profile_photo_data=json.loads(profile_photo)
					if profile_photo_data:
							
						filenamess=profile_photo_data['filename']
						filetypess=profile_photo_data['filetype']
						
						base641ss=profile_photo_data['base64']

						img_name='User'+str(get_user_register_instance)+'_photo'

						image_encoded=base64.b64decode(base641ss)

						status=uploading_file(bucket,"Users_Documents",img_name+'.png',image_encoded,'image/png')
						log.info(status)

						if status==True:
							update_profile_path=UserRegisterationDetails.objects.get(id=get_user_register_instance)
								
							update_profile_path.image=img_name+".png"
							update_profile_path.save()

						else:
							result['a_res']='2'
					else:
						outfile=''
						log.info("same")
					
					result['a_res']='0'
				else:
					result['a_res']='1'
				

				result['status']='success'
			else:
				
				result['status']='exists'
					
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)

@csrf_exempt
def check_login_user_credentials(request):
	login_result={}
	try:
		req_data=request.POST.get('data')
		req_data=json.loads(req_data)
		
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)
	try:
		if request.method=="POST":
			
			if request.method=="POST":
				if User.objects.filter(username=req_data['email_login']).exists():
					user= authenticate(username=req_data['email_login'], password=req_data['password'])
					log.info(user)
					if(user!=None):
						login(request, user)
						current_user = request.user

						get_user_id = User.objects.get(username=req_data['email_login']).id
						
						if UserRegisterationDetails.objects.filter(auth_user=get_user_id).exists():
							
							user_id=UserRegisterationDetails.objects.get(auth_user=get_user_id).id
							
							get_session_key=request.session.session_key
							request.session.flush()

							if 'user_id' in request.session:
								request.session['user_id'] = user_id
							else:
								request.session['user_id'] = user_id

							
							login_result['user_id']=user_id
							
							login_result['status']='success'

						else:
						
							login_result['status']="wrong"
							
					else:
						
						login_result['status']="wrong"
				else:
					login_result['status']="email_mobile"
			
		return HttpResponse(json.dumps(login_result), content_type='application/json')
	except Exception as ex:
					log.error('Error in json data: %s' % ex) 




@csrf_exempt
def logout_view(request):
	try: 
		
		get_session_key=request.session.session_key
		log.info(get_session_key)
		request.session.flush()
		

		return redirect('http://'+request.get_host()+'/login')

	except Exception as ex: 
		log.error('Error in Logout API: req_data: %s' % ex)


@csrf_exempt
def DashboardPageOnload(request):
	try:

		
		get_key = request.session.has_key('user_id')
		
		if get_key!=False:
			data_get=request_session_check(request)
			data_values=data_get.split("|")
			
			return render(request,'dashboard.html',{
				'get_firstname':data_values[0].title(),
				'user_id':data_values[1],
				"current_date":data_values[2],
			})
		else:
			
			if request.user.is_anonymous==False:
				get_firstname=request.user
				get_current_date_value=current_time_function()
				return render(request,'dashboard.html',{
					'get_firstname':get_firstname,
					"current_date":get_current_date_value
				}) 
			else:
				return redirect('http://'+request.get_host()+'/')

	except Exception as ex:
			log.error('Error in json data: %s' % ex)

def current_time_function():
	get_server_current_time= timezone.now()
	get_current_timestamp= get_server_current_time +datetime.timedelta(hours=5,minutes=30)
	get_format_current_date=str(get_current_timestamp)[:19]
	get_current_timestamp_new = datetime.datetime.strptime(get_format_current_date, '%Y-%m-%d %H:%M:%S')
	get_current_date=get_current_timestamp_new.date()
	get_month=get_current_date.strftime('%B') 
	day=get_current_date.day
	day_double=str(day).zfill(2)
	year=get_current_date.year
	get_current_date_value=str(day_double)+' '+str(get_month)+' '+str(year)
	return get_current_date_value

@csrf_exempt
def ChangePasswordOnload(request):
	try:
		
		get_key = request.session.has_key('user_id')
		
		if get_key!=False:
			data_get=request_session_check(request)
			data_values=data_get.split("|")
			
			return render(request,'change_password.html',{
				'get_firstname':data_values[0].title(),
				'user_id':data_values[1],
				"current_date":data_values[2],
			})
		else:
			 return redirect('http://'+request.get_host()+'/dashboard')
	except Exception as ex:
			log.error('Error in json data: %s' % ex)

@csrf_exempt
def date_time_value_function(request,data):
	result={}
	try:
		req_data=json.loads(data)
		
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
	try:
		if request.method=='POST':
			
			if req_data['get_hours'] >= 4 and req_data['get_hours']<12:
				greeting_name    = "Good Morning"; 
			elif req_data['get_hours'] >= 12 and req_data['get_hours']<16:
				greeting_name   = "Good Afternoon";
			elif req_data['get_hours'] >= 16 and req_data['get_hours']<20: 
				greeting_name    = "Good Evening";
			elif req_data['get_hours'] >= 20 and req_data['get_hours']<4:
				greeting_name    = "Peaceful Night";
			else:
			   greeting_name="Be Relax - Be Happy"; 
			result['greeting_name']=greeting_name
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)

@csrf_exempt
def verification_old_password_system(request,data):
	result={}
	try:
		req_data=json.loads(data)
		
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
	try:
		if request.method=='POST':
			if UserRegisterationDetails.objects.filter(id=req_data['user_id']).exists():
				get_pssword_data_value=UserRegisterationDetails.objects.get(id=req_data['user_id']).password
				if get_pssword_data_value==req_data['password_value']:
					result['status1']='valid'
				else:
					result['status1']='notvalid'

				result['status']='0'
			else:
				result['status']='1'
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)

@csrf_exempt
def update_user_password(request):
	result={}
	try:
		req_data=request.POST.get('data')
		req_data=json.loads(req_data)
		
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)
	try:
		if request.method=="POST":
			
			if UserRegisterationDetails.objects.filter(id=req_data['user_id']).exists():
				get_instance=UserRegisterationDetails.objects.get(id=req_data['user_id'])
				get_instance.password=req_data['password']
				get_instance.save()

				get_auth_data=get_instance.auth_user.id
				get_user_update=User.objects.get(id=get_auth_data)
				new_password=get_user_update.set_password(req_data['password'])
				get_user_update.save()
				result['status']='success'
			else:
				result['status']='error'
		
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)